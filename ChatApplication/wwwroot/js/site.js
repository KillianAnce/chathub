﻿'use strict';

// Création de la connexion
var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');

var stompClient = null;
var username = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];


// Lance une connexion
connection.start().then(function () {
}).catch(function (err) {
    return console.error(err.toString());
});

function connect(event) {
    username = document.getElementById("userName").value;
    usernamePage.classList.add('hidden');
    chatPage.classList.remove('hidden');
    connectingElement.classList.add('hidden');
    event.preventDefault();
    //TODO: Envoyer un message de type JOIN grâce à la variable connection et la méthode invoke.
    connection.invoke("sendMessage", username, "JOIN");
    //connection.invoke(...)
}




document.getElementById("sendMessage").addEventListener("click", event => {
    var messageContent = messageInput.value.trim();
    username = document.getElementById("userName").value;
    if (messageContent) {
        //TODO: Envoyer le contenu du message grâce à la variable connection et la méthode invoke.
        //connection.invoke(...)
        connection.invoke("sendMessage", username, messageContent);
        messageInput.value = '';
    }
    event.preventDefault();
})


connection.on("ReceiveMessage", function (user, message) {
    var messageElement = document.createElement('li');
    if (message === "JOIN") {
        messageElement.classList.add('event-message');
        message = user + ' joined!';
    } else if (message === 'LEAVE') {
        messageElement.classList.add('event-message');
        message = user + ' left!';
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(user[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(user);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(user);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
})


function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }

    var index = Math.abs(hash % colors.length);
    return colors[index];
}



usernameForm.addEventListener('submit', connect, true)